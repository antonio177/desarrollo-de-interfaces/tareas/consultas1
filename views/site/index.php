<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Selección</h1>
    </div>

    <div class="body-content">
        <!-- Inicio de la primera fila -->

        <div class="row">
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 1 </h4>
                        <p> Listar las edades de los ciclistas sin repetidos </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        
    
    
    
        <!-- Inicio de la primera fila -->

        
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 2 </h4>
                        <p> Listar las edades de los ciclistas de Artiach </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        
    
    
   
        <!-- Inicio de la primera fila -->

        
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 3 </h4>
                        <p> Listar las edades de los ciclistas de Artiach o de Amore Vita </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        </div>
    </div>
    
    <div class="body-content">
        <!-- Inicio de la primera fila -->

        <div class="row">
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 4 </h4>
                        <p> Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30 </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        
    
    
    
        <!-- Inicio de la primera fila -->

        
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 5 </h4>
                        <p> Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        
    
    
    
        <!-- Inicio de la primera fila -->

        
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 6 </h4>
                        <p> Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8 </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        </div>
    </div>
    
    <div class="body-content">
        <!-- Inicio de la primera fila -->

        <div class="row">
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 7 </h4>
                        <p> Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        
    
    
    
        <!-- Inicio de la primera fila -->

        
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 8 </h4>
                        <p> Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        
    
    
    
        <!-- Inicio de la primera fila -->

        
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 9 </h4>
                        <p> Listar el nombre de los puertos cuya altura sea mayor de 1500 </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        </div>
    </div>
    
    <div class="body-content">
        <!-- Inicio de la primera fila -->

        <div class="row">
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-7 col-md-5">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 10 </h4>
                        <p> Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000 </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        
    
    
    
        <!-- Inicio de la primera fila -->

        
            <!-- 
            Botón de consulta
            -->
            <div class="col-sm-7 col-md-5">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 11 </h4>
                        <p> Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000 </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>            
        </div>
    </div>
    
</div>