<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Maillot $model */

$this->title = 'Update Maillot: ' . $model->código;
$this->params['breadcrumbs'][] = ['label' => 'Maillots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->código, 'url' => ['view', 'código' => $model->código]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maillot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
